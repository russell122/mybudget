import Vue from "vue";
import Vuex from "vuex";

import users from "./modules/users";
import posts from "./modules/posts";
import income from "./modules/income";
import expenses from "./modules/expenses";
import fullbalans from "./modules/fullbalans";
import optionsCategory from "./modules/optionsCategory";
import allIncome from "./modules/allIncome";
import allExpenses from "./modules/allExpenses";
import authUser from "./modules/authUser";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {},
	mutations: {},
	actions: {},
	modules: {
		users,
		posts,
		income,
		expenses,
		fullbalans,
		optionsCategory,
		allIncome,
		allExpenses,
		authUser,
	}
})