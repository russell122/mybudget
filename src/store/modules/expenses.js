import Vue from 'vue';
import { calcSumExpOrIncForMonth, getArrForYear } from "@/scripts/main.js";

const expensesStore = {
	namespaced: true,
	state: {
		currentExpenses: {},
		expenses: {
			2019: {},
			2020: {
				1: {
					category: 'Гаджеты',
					currency: "RUB",
					value: 100000,
					commentary: 'Ноутбук',
					dataFull: 'Thu Jan 28 2020 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
			},
			2021: {
				1: {
					category: 'Продукты',
					currency: "RUB",
					value: 1000,
					commentary: 'Пицца',
					dataFull: 'Tue Nov 01 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				2: {
					category: 'Гаджеты',
					currency: "RUB",
					value: 30000,
					commentary: 'Мобильный телефон',
					dataFull: 'Web Nov 10 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				3: {
					category: 'Продукты',
					currency: "RUB",
					value: 100,
					commentary: 'Чипсы',
					dataFull: 'Sun Nov 21 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				4: {
					category: 'Продукты',
					currency: "RUB",
					value: 50,
					commentary: 'Булочка',
					dataFull: 'Sun Nov 21 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				5: {
					category: 'Продукты',
					currency: "RUB",
					value: 500,
					commentary: 'Пирожок',
					dataFull: 'Mon Nov 22 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				6: {
					category: 'Продукты',
					currency: "RUB",
					value: 1500,
					commentary: 'Роллы',
					dataFull: 'Thu Aug 01 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
			},
			2022: {},
		},
		optionsCategory: [
			{ text: "Продукты", value: "Продукты" },
			{ text: "Гаджеты", value: "Гаджеты" },
		],
		currentYear: (new Date()).getFullYear(),
		currentMonth: (new Date().getMonth() + 1),
	},
	getters: {
		getExpenses(state) {
			// console.log(state.expenses)
			// Прокидываю данные в виде массива с объектами
			if (Object.values(state.expenses).length >= 1) {
				return getArrForYear(state.expenses, state.currentYear, state.currentExpenses);
			} else {
				return 0;
			}

		},

		getExpensesAmountForToday(state, getters) {
			// Расчет расходов за сегодня в текущем году
			const currentDate = new Date().toLocaleDateString();
			let sum = 0;

			Object.values(getters.getExpenses).forEach(el => {
				if (currentDate == new Date(el.dataFull).toLocaleDateString()) {
					sum += el.value;
				}
			})
			return sum;
		},
		getExpensesAmountForWeek(state, getters) {
			// Расчет расходов за неделю в текущем году
			let date = new Date();

			let arr = [];
			Object.values(getters.getExpenses).forEach(el => {
				if (date.getWeek() == date.getWeekEl(new Date(el.dataFull))) {
					arr.push(el)
				}
			})

			let sum = 0;
			arr.forEach(el => {
				sum += el.value;
			})
			return sum;
		},
		getExpensesAmountForMonth(state, getters) {
			// Расчет расходов за месяц в текущем году
			if (getters.getExpenses >= 1) {
				return calcSumExpOrIncForMonth(getters.getExpenses, state.currentMonth);
			} else {
				return 0;
			}

		},
	},
	mutations: {
		ADD_NEW_EXPENSES(state, expensesAct) {
			// Записываю новый расход в исходный объект
			Vue.set(state.expenses[new Date(expensesAct.dataFull).getFullYear()], expensesAct.id, expensesAct);
		},
	},
	actions: {
		setNewExpenses(context, newExpenses) {
			// Получаю данные из формы введенные пользователем, добавляю им id и прокидываю дальше в мутации для дальнейшей записи в исходный объект
			const newExpensesPlusId = { ...newExpenses, id: Object.values(context.state.expenses[new Date(newExpenses.dataFull).getFullYear()]).length + 1 };
			context.commit('ADD_NEW_EXPENSES', newExpensesPlusId);
		},
	}
};

export default expensesStore;