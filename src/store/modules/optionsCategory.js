import Vue from 'vue';


const optionsCategory = {
	namespaced: true,
	state: {

	},
	getters: {
		getOptionsCategoryInc(state, getters, rootState) {
			// Прокидываю категории в селекте доходов
			return rootState.income.optionsCategory;
		},
		getOptionsCategoryExp(state, getters, rootState) {
			// Прокидываю категории в селекте расходов
			return rootState.expenses.optionsCategory;
		},
	},
	mutations: {

	},
	actions: {

	}
};

export default optionsCategory;