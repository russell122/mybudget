import Vue from 'vue';
import { calcSumExpOrIncForMonth, getArrForYear } from "@/scripts/main.js";
import { getDatabase, ref, set, update, push, child } from "firebase/database";
import { database } from "@/main";

const incomeStore = {
	namespaced: true,
	state: {
		currentIncome: {},
		income: {
			2019: {},
			// 2020: {
			// 1: {
			// 	category: 'Зарплата',
			// 	currency: "RUB",
			// 	value: 40000,
			// 	commentary: 'Зарплата',
			// 	dataFull: 'Thu Nov 08 2020 10:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// 2: {
			// 	category: 'Аванс',
			// 	currency: "RUB",
			// 	value: 30000,
			// 	commentary: 'Аванс',
			// 	dataFull: 'Thu Jan 30 2020 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// },
			// 2021: {
			// 1: {
			// 	category: 'Зарплата',
			// 	currency: "RUB",
			// 	value: 40000,
			// 	commentary: 'Зарплата',
			// 	dataFull: 'Thu Nov 11 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// 2: {
			// 	category: 'Аванс',
			// 	currency: "RUB",
			// 	value: 20000,
			// 	commentary: 'Аванс',
			// 	dataFull: 'Tue Nov 30 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// 3: {
			// 	category: 'Аванс',
			// 	currency: "RUB",
			// 	value: 20000,
			// 	commentary: 'Аванс',
			// 	dataFull: 'Mon Aug 30 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// 4: {
			// 	category: 'Аванс',
			// 	currency: "RUB",
			// 	value: 30000,
			// 	commentary: 'Аванс',
			// 	dataFull: 'Mon Nov 08 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// 5: {
			// 	category: 'Аванс',
			// 	currency: "RUB",
			// 	value: 20000,
			// 	commentary: 'Аванс',
			// 	dataFull: 'Fri Nov 05 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
			// },
			// },
			2022: {},
		},
		incomeTT: {
			2019: {},
			2020: {
				1: {
					category: 'Зарплата',
					currency: "RUB",
					value: 40000,
					commentary: 'Зарплата',
					dataFull: 'Thu Nov 08 2020 10:00:00 GMT+0300 (Москва, стандартное время)',
				},
				2: {
					category: 'Аванс',
					currency: "RUB",
					value: 30000,
					commentary: 'Аванс',
					dataFull: 'Thu Jan 30 2020 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
			},
			2021: {
				1: {
					category: 'Зарплата',
					currency: "RUB",
					value: 40000,
					commentary: 'Зарплата',
					dataFull: 'Thu Nov 11 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				2: {
					category: 'Аванс',
					currency: "RUB",
					value: 20000,
					commentary: 'Аванс',
					dataFull: 'Tue Nov 30 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				3: {
					category: 'Аванс',
					currency: "RUB",
					value: 20000,
					commentary: 'Аванс',
					dataFull: 'Mon Aug 30 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				4: {
					category: 'Аванс',
					currency: "RUB",
					value: 30000,
					commentary: 'Аванс',
					dataFull: 'Mon Nov 08 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
				5: {
					category: 'Аванс',
					currency: "RUB",
					value: 20000,
					commentary: 'Аванс',
					dataFull: 'Fri Nov 05 2021 00:00:00 GMT+0300 (Москва, стандартное время)',
				},
			},
			2022: {},
		},
		optionsCategory: [
			{ text: "Зарплата", value: "Зарплата" },
			{ text: "Премия", value: "Премия" },
			{ text: "Аванс", value: "Аванс" },
		],
		currentYear: (new Date()).getFullYear(),
		currentMonth: (new Date().getMonth() + 1),

	},
	getters: {
		getIncomeTT(state) {
			return state.incomeTT;
		},
		getIncomeTT2(state) {
			return state.income;
		},
		getIncome(state) {
			// Прокидываю данные в виде массива с объектами
			console.log(Object.values(state.income).length)
			console.log(state.income)
			// if (Object.values(state.income).length >= 1) {
			return getArrForYear(state.income, state.currentYear, state.currentIncome);
			// } else {
			// return 0;
			// }

		},
		getIncomeAmount(state, getters) {
			// Расчет доходов за месяц в текущем году
			// console.log(getters.getIncome)
			// if (getters.getIncome >= 1) {
			return calcSumExpOrIncForMonth(getters.getIncome, state.currentMonth);
			// } else {
			// return 0;
			// }

		},
	},
	mutations: {
		ADD_NEW_INCOME(state, incomeAct) {
			// Записываю новый доход в исходный объект
			// Vue.set(state.income[new Date(incomeAct.dataFull).getFullYear()], incomeAct.id, incomeAct, incomeAct.userName, incomeAct.year);

			console.log(incomeAct)
			console.log(incomeAct.userName)
			console.log(incomeAct.date)

			console.log(new Date(incomeAct.dataFull).getFullYear())


			set(ref(database, `${incomeAct.userName}/` + '/income/' + new Date(incomeAct.dataFull).getFullYear() + '/' + `${incomeAct.id}/`), {
				category: incomeAct.category,
				currency: incomeAct.currency,
				value: incomeAct.value,
				commentary: incomeAct.сommentary,
				dataFull: incomeAct.dataFull,
			})
				.then((data) => {
					// console.log('Зашел')
					// console.log(data)
				})
				.catch((error) => {
					// console.log(error)
				});

			// set(ref(database, incomeAct.userName + '/' + 'income' + '/' + incomeAct.year + '/' + incomeAct.id), {
			// 	username: 'name',
			// 	email: 'email',
			// 	profile_picture: 'imageUrl'
			// })
			// 	.then((data) => {
			// 		console.log('Зашел')
			// 		console.log(data)
			// 	})
			// 	.catch((error) => {
			// 		console.log(error)
			// 	})


			// const updates = {};
			// console.log(incomeAct.userName)
			// console.log(incomeAct.year)
			// updates['/t/' + 'income' + '/' + 2019 + '/' + incomeAct.id] = incomeAct;
			// updates['/' + `${incomeAct.userName}` + '/' + 'income' + '/' + `${incomeAct.year}` + '/' + `${incomeAct.id}`] = incomeAct;

			// return update(ref(database), updates);

		},

	},
	actions: {
		setNewIncome(context, newIncome, rootState) {
			// Получаю данные из формы введенные пользователем, добавляю им id и прокидываю дальше в мутации для дальнейшей записи в исходный объект

			// console.log(context.state)
			// console.log(context.state)

			const newIncomePlusId = { ...newIncome, id: Object.values(context.state.income[new Date(newIncome.dataFull).getFullYear()]).length + 1, userName: context.rootState.authUser.userName, year: new Date(newIncome.dataFull).getFullYear() };
			context.commit('ADD_NEW_INCOME', newIncomePlusId);
			// context.commit('ADD_NEW_INCOME', newIncome);
		},
	}
};

export default incomeStore;