import Vue from 'vue';


const allExpenses = {
	namespaced: true,
	state: {
		currentYear: (new Date()).getFullYear(),
		yearSelectedByTheUser: (new Date()).getFullYear(),
		monthSelectedByTheUser: (new Date()).getMonth() + 1,
		daySelectedByTheUser: (new Date()).getDate(),
		startWeekSelectedByTheUser: new Date(),
		startRangeDateSelectedByTheUser: 0,
		endRangeDateSelectedByTheUser: 0,
		changeType: 'Месяц',

		monthSelectedByTheUserFullDate: new Date(),
		daySelectedByTheUserFullDate: new Date(),
		startWeekSelectedByTheUserFullDate: new Date(),
		yearSelectedByTheUserFullDate: (new Date()).getFullYear(),
		fullRangePeriod: '',
	},
	getters: {
		getFullRangePeriod(state) {
			// Возвращаю выбранный юзером период
			return state.fullRangePeriod;
		},
		getCurrentYear(state) {
			// Возвращаю текущий год
			return state.currentYear;
		},

		getFullCost(state, getters, rootState) {
			// Полная сумма расходов за всё время
			let arr = [];
			Object.values(rootState.expenses.expenses).forEach(el => {
				arr.push(Object.values(el).reduce((sum, elem) => sum + elem.value, 0));
			})
			return arr.reduce((sum, elem) => sum += elem);
		},
		getFullExpensesValueCategory(state, getters) {
			return Object.values(getters.getFullExpensesForEachCategory).reduce((sum, el) => sum + el, 0)
		},
		getFullExpensesForEachCategory(state, getters, rootState) {
			let arr = [];
			let obj = {};

			function formingObjectWithData(arr) {
				arr.forEach(el => {
					if (obj[el.category] == undefined) {
						obj[el.category] = el.value;
					} else {
						obj[el.category] = obj[el.category] + el.value;
					}
				})
			}

			if (state.changeType == 'Неделя') {
				let date = new Date();
				arr = [];

				let year = state.monthSelectedByTheUserFullDate.getFullYear();

				Object.values(rootState.expenses.expenses).forEach(elem => {
					Object.values(elem).forEach(el => {
						if (year == new Date(el.dataFull).getFullYear() && date.getWeekEl(new Date(state.startWeekSelectedByTheUser)) == date.getWeekEl(new Date(el.dataFull))) {
							arr.push(el);
						}
					})
				})

			} else if (state.changeType == 'День') {
				arr = [];
				let year = state.monthSelectedByTheUserFullDate.getFullYear();
				let month = state.daySelectedByTheUserFullDate.getMonth() + 1;

				Object.values(rootState.expenses.expenses).forEach(elem => {
					Object.values(elem).forEach(el => {
						if (year == new Date(el.dataFull).getFullYear() && month == new Date(el.dataFull).getMonth() + 1 && state.daySelectedByTheUser == new Date(el.dataFull).getDate()) {
							arr.push(el)
						}
					})
				})

			}
			else if (state.changeType == 'Месяц') {
				arr = [];
				let year = state.monthSelectedByTheUserFullDate.getFullYear();

				Object.values(rootState.expenses.expenses).forEach(el => {
					Object.values(el).forEach(elem => {
						if (year == new Date(elem.dataFull).getFullYear() && state.monthSelectedByTheUser == new Date(elem.dataFull).getMonth() + 1) {
							arr.push(elem)
						}
					})
				})

			}
			else if (state.changeType == 'Год') {
				arr = [];
				let year = state.yearSelectedByTheUserFullDate;

				Object.values(rootState.expenses.expenses).forEach(elem => {
					Object.values(elem).forEach(el => {
						if (year == new Date(el.dataFull).getFullYear()) {
							arr.push(el)
						}
					})
				})

			}
			else if (state.changeType == 'Период') {
				arr = [];
				let res = [];

				let startDateYear = new Date(state.startRangeDateSelectedByTheUser).getFullYear();
				let startDateMonth = new Date(state.startRangeDateSelectedByTheUser).getMonth() + 1;
				let startDateDays = new Date(state.startRangeDateSelectedByTheUser).getDate();

				let endDateYear = new Date(state.endRangeDateSelectedByTheUser).getFullYear();
				let endDateMonth = new Date(state.endRangeDateSelectedByTheUser).getMonth() + 1;
				let endDateDays = new Date(state.endRangeDateSelectedByTheUser).getDate();

				Object.keys(rootState.expenses.expenses).forEach(elem => {
					if (+elem >= +startDateYear && +elem <= endDateYear) {
						if (Object.keys(rootState.expenses.expenses[elem]).length >= 1) {
							res.push(rootState.expenses.expenses[elem]);
						}

					}
				})

				res.forEach(elem => {
					Object.values(elem).forEach(el => {
						if (new Date(el.dataFull).getMonth() + 1 >= +startDateMonth && new Date(el.dataFull).getMonth() + 1 <= +endDateMonth) {
							if (new Date(el.dataFull).getDate() >= +startDateDays && new Date(el.dataFull).getDate() <= +endDateDays) {
								arr.push(el)
							}
						}
					})
				})
			}
			else if (state.changeType == 'fullTime') {
				Object.values(rootState.expenses.expenses).forEach(el => {
					Object.values(el).forEach(elem => {
						arr.push(elem);
					})
				})
			}
			formingObjectWithData(arr);
			return obj;
		},

	},
	mutations: {
		ADD_NEW_YEAR(state, year) {
			state.yearSelectedByTheUser = year;
			state.yearSelectedByTheUserFullDate = year;
		},
		ADD_NEW_MONTH(state, arr) {
			[state.yearSelectedByTheUser, state.monthSelectedByTheUser, state.monthSelectedByTheUserFullDate] = arr;
		},
		ADD_NEW_DAY(state, arr) {
			[state.yearSelectedByTheUser, state.monthSelectedByTheUser, state.daySelectedByTheUser, state.daySelectedByTheUserFullDate] = arr;
		},
		ADD_NEW_WEEK(state, arr) {
			[state.yearSelectedByTheUser, state.monthSelectedByTheUser, state.startWeekSelectedByTheUser] = arr;
			state.startWeekSelectedByTheUserFullDate = arr[2];
		},
		ADD_NEW_CANGE_TYPE(state, type) {
			state.changeType = type;
		},
		ADD_NEW_RANGE(state, arr) {
			state.fullRangePeriod = arr;
			[state.startRangeDateSelectedByTheUser, state.endRangeDateSelectedByTheUser] = arr;
		},
	},
	actions: {
		setNextYearExp(context, year) {
			context.commit('ADD_NEW_YEAR', year);
		},
		setPreviousYearExp(context, year) {
			context.commit('ADD_NEW_YEAR', year);
		},
		setNewChangeGapType(context, newType) {
			context.commit('ADD_NEW_CANGE_TYPE', newType);
		},
		setNextYearInc(context, year) {
			context.commit('ADD_NEW_YEAR', year);
		},
		setPreviousYearInc(context, year) {
			context.commit('ADD_NEW_YEAR', year);
		},
		setPrevMonth(context, month) {
			context.commit('ADD_NEW_MONTH', month);
		},
		setPrevDay(context, day) {
			context.commit('ADD_NEW_DAY', day);
		},
		setPrevWeek(context, week) {
			context.commit('ADD_NEW_WEEK', week);
		},
		setPrevRange(context, arr) {
			context.commit('ADD_NEW_RANGE', arr);
		},
	}
};

export default allExpenses;