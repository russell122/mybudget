import Vue from 'vue';

const usersStore = {
	namespaced: true,
	state: {
		list: {
			1: {
				name: 'Alex',
				age: 31,
			}
		}
	},
	getters: {
		usersList: ({ list }) => Object.values(list),
	},
	mutations: {
		ADD_USER(state, user) {
			Vue.set(state.list, user.id, user);

			// console.log(state)
			// console.log(user)
		}
	},
	actions: {
		addNewUser(context, user) {
			const newUser = { ...user, id: String(Math.random()) };
			context.commit('ADD_USER', newUser);

			// console.log(newUser)
			// console.log(context)
		}
	}
};

export default usersStore;