import Vue from 'vue';
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "firebase/auth";
import { getDatabase, ref, onValue } from "firebase/database";
import { database } from "@/main";


const authUser = {
	namespaced: true,
	state: {
		statusUser: false,
		userName: '',
	},
	getters: {
		getStatusUser(state) {
			const auth = getAuth();

			onAuthStateChanged(auth, (user) => {
				if (user) {
					// console.log(user.email)
					const uid = user.uid;
					state.userName = user.email.split('@')[0];
					state.statusUser = uid;
				} else {
					console.log('Не авторизованный пользователь')
				}
			});

			return state.statusUser;
		},
		getUserName(state) {
			return state.userName;
		},
		getExpensesFromDB(state, getters, rootState, rootGetters) {
			const starCountRef = ref(database, state.userName);
			onValue(starCountRef, (snapshot) => {
				const myData = snapshot.val();
				// updateStarCount(postElement, myData);

				return rootState.expenses.expenses = myData.expenses;

				// if (myData != undefined || myData != null) {
				// 	return rootState.expenses.expenses = myData.expenses;
				// } else {
				// 	return rootState.expenses.expenses = { 2021: {} };
				// }

			});
		},
		getIncomeFromDB(state, getters, rootState, rootGetters) {
			const starCountRef = ref(database, state.userName);
			onValue(starCountRef, (snapshot) => {
				const myData = snapshot.val();
				// updateStarCount(postElement, myData);
				console.log(myData)

				return rootState.income.income = myData.income;

				// if (myData != undefined || myData != null) {
				// 	return rootState.income.income = myData.income;
				// } else {
				// 	return rootState.income.income = { 2021: {} };
				// }

			});
		},
	},
	mutations: {
		ADD_NEW_STATUS_USER(state, status) {
			state.statusUser = status;
		},
		ADD_NEW_USER_NAME(state, name) {
			state.userName = name;
		}
	},
	actions: {
		setStatusUser(context, status) {
			context.commit('ADD_NEW_STATUS_USER', status);
		},
		setUserName(context, name) {
			context.commit('ADD_NEW_USER_NAME', name);
		},
	}
};

export default authUser;