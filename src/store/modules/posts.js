// import Vue from 'vue';

const postsStore = {
	namespaced: true,
	state: {
		posts: [],
	},
	getters: {
		allPosts(state) {
			return state.posts;
		},
		postsCount(state) {
			return state.posts.length
		}
	},
	mutations: {
		UPDATE_POSTS(state, servPosts) {
			state.posts = servPosts;
		},
		CREATE_POST(state, newPost) {
			state.posts.push(newPost);
		}
	},
	actions: {
		async fetchPosts(context, limit = 3) {
			const res = await fetch(
				"https://jsonplaceholder.typicode.com/posts?_limit=" + limit
			);
			const servPosts = await res.json();
			context.commit('UPDATE_POSTS', servPosts);
		}
	}
};

export default postsStore;