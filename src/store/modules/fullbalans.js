import Vue from 'vue';
import { calcFullIncAndExpMonth, calcFullCostandIncForYears, getObjNeedsUserAYear } from "@/scripts/main.js";
// import { calcFullCostandIncForYears } from "@/scripts/main.js";

const fullbalansStore = {
	namespaced: true,
	state: {
		yearBalanceForYears: (new Date()).getFullYear(),
		currentExpenses: {},
		currentIncome: {},
	},
	getters: {
		getFullIncomeArrMonths(state, getters, rootState, rootGetters) {
			// Полная сумма доходов за каждый месяц
			// return calcFullIncAndExpMonth(rootGetters['income/filterArr']);
			return calcFullIncAndExpMonth(getters.filterArrInc);
		},
		getFullExpensesArrMonths(state, getters, rootState, rootGetters) {
			// Полная сумма расходов за каждый месяц
			// return calcFullIncAndExpMonth(rootGetters['expenses/filterArr']);
			return calcFullIncAndExpMonth(getters.filterArrExp);
		},
		getFullIncomeYears(state, getters, rootState, rootGetters) {
			// Полная сумма доходов за год
			// return calcFullCostandIncForYears(rootGetters['income/filterArr']);
			return calcFullCostandIncForYears(getters.filterArrInc);
		},
		getFullCostYears(state, getters, rootState, rootGetters) {
			// Полная сумма расходов за год
			// return calcFullCostandIncForYears(rootGetters['expenses/filterArr']);
			return calcFullCostandIncForYears(getters.filterArrExp);
		},
		getYearBalanceForYears(state) {
			// Возвращаю год который выбрал пользователь на странице "Баланс за год"
			return state.yearBalanceForYears;
		},
		filterArrInc(state, getters, rootState) {
			// Возвращаю все доходы за нужный юзеру год
			return getObjNeedsUserAYear(rootState.income.income, state.yearBalanceForYears, state.currentIncome);
		},
		filterArrExp(state, getters, rootState) {
			// Возвращаю все расходы за нужный юзеру год
			return getObjNeedsUserAYear(rootState.expenses.expenses, state.yearBalanceForYears, state.currentExpenses);
		},
		getBalanceMonth(state, getters) {
			// Нахожу баланс за каждый месяц между доходами и расходами
			let rez = [];
			getters.getFullIncomeArrMonths.forEach((elem, i) => {
				Object.values(elem).reduce((sum, el, k) => {
				sum = el - Object.values(getters.getFullExpensesArrMonths[i])[k];
				rez.push(sum);
				}, 0)
			});
			return rez;
		},
	},
	mutations: {
		ADD_NEW_YEAR(state, year) {
			// Перезаписываю год
			state.yearBalanceForYears = year;
		},
	},
	actions: {
		setPreviousYearFull(context, year) {
			// Прокидываю дату на год раньше
			context.commit('ADD_NEW_YEAR', year)
		},
		setNextYearFull(context, year) {
			// Прокидываю дату на год дальше
			context.commit('ADD_NEW_YEAR', year)
		},
	}
};

export default fullbalansStore;