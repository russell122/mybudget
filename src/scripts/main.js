
window.addEventListener('DOMContentLoaded', () => {
	// фулл скрин

	let vh = window.innerHeight * 0.01;
	document.documentElement.style.setProperty('--vh', `${vh}px`);

	window.addEventListener('resize', () => {
		// We execute the same script as before
		let vh = window.innerHeight * 0.01;
		document.documentElement.style.setProperty('--vh', `${vh}px`);
	});

	// конец фулл скрина

});

// -- Логика для fullbalans --

function calcFullIncAndExpMonth(linkBbj) {
	// Вычисляю сумму доходов/расходов за каждый месяц и возвращаю массив с данными
	let arr = [];
	let arrMonth = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

	function calcValue(value) {
		let sum = 0;
		let obj = {};

		Object.values(linkBbj).forEach(el => {
			if (new Date(el.dataFull).getMonth() + 1 == value) {
				sum += el.value;
			}
		})
		obj[value] = sum;
		return obj;
	}

	arrMonth.forEach(el => {
		arr.push(calcValue(el));
	})

	return arr;

}

function calcFullCostandIncForYears(linkBbj) {
	// Вычисляю полную сумму доходов/расходов за год
	return Object.values(linkBbj).reduce((sum, el) => sum += el.value, 0);
}

function getObjNeedsUserAYear(obj, year, resObj) {
	// Возвращаю все доходы/расходы за нужный юзеру год
	Object.keys(obj).forEach(el => {
		if (el == year) {
			resObj = obj[el]
		}
	})
	return resObj;
}

// -- end fullbalans --

// -- Логика для IncomeCategorySettings --

// Проверка есть ли такой эл. в категориях при вводе новой и редактировании
function searchForAlreadyExistingCategories(elems, input) {
	return !elems.some(el => el.text.trim() === input.trim());
}

// -- end IncomeCategorySettings --

// -- Логика для AddExpenses и AddIncome --
function addNewEntry(newObj) {
	let dataFull = JSON.parse(JSON.stringify(newObj.date1));
	newObj.date = newObj.date1.toLocaleString();
	newObj.dateTime = newObj.date2.toLocaleString();
	let newObjFull = {
		...newObj,
		dataFull,
	};
	return newObjFull;
}
// -- end AddExpenses и AddIncome --

// -- Логика для expenses и income --

function getArrForYear(obj, year, resObj) {
	// Прокидываю данные в виде массива с объектами
	Object.keys(obj).forEach(el => {
		if (el == year) {
			resObj = obj[el]
		}
	})

	return resObj;
}

function calcSumExpOrIncForMonth(obj, currentMonth) {
	// Расчет доходов и расходов за месяц в текущем году
	let res = Object.values(obj).reduce((sum, el) => {
		if (currentMonth == new Date(el.dataFull).getMonth() + 1) {
			sum += el.value;
		}
		return sum;
	}, 0)

	return res;
}

// -- end expenses и income --

Date.prototype.getWeek = function () {
	var date = new Date(this.getTime());
	date.setHours(0, 0, 0, 0);
	date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	var week1 = new Date(date.getFullYear(), 0, 4);
	return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
		- 3 + (week1.getDay() + 6) % 7) / 7);
}

Date.prototype.getWeekEl = function (dataEl) {
	var date = dataEl;
	date.setHours(0, 0, 0, 0);
	date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
	var week1 = new Date(date.getFullYear(), 0, 4);
	return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
		- 3 + (week1.getDay() + 6) % 7) / 7);
}

export { calcFullIncAndExpMonth, calcFullCostandIncForYears, getObjNeedsUserAYear, searchForAlreadyExistingCategories, addNewEntry, calcSumExpOrIncForMonth, getArrForYear };
