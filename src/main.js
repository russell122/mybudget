import Vue from 'vue'
import App from './App.vue'
import './plugins/elements';
import store from "./store";
import VueRouter from 'vue-router';
import router from './router/router';
import vuetify from './vuetify/vuetify';
import dayjs from 'dayjs';

import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onValue } from "firebase/database"
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyBXWersoXhNIa1zyVpYMw_KDoH2C65WwK0",
  authDomain: "mybudget-8940a.firebaseapp.com",
  databaseURL: "https://mybudget-8940a-default-rtdb.firebaseio.com",
  projectId: "mybudget-8940a",
  storageBucket: "mybudget-8940a.appspot.com",
  messagingSenderId: "528023539935",
  appId: "1:528023539935:web:528f0665d4cee9253bd96a",
  measurementId: "G-SLV0SCNVLF"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);



// let appWrap;

// const auth = getAuth();
// onAuthStateChanged(auth, (user) => {
//   if (user) {
//     // if (!appWrap) {
//     //   appWrap = new Vue({
//     //     store,
//     //     router,
//     //     vuetify,
//     //     provide() {
//     //       return {
//     //         $dayjs: dayjs
//     //       }
//     //     },
//     //     render: h => h(App),
//     //   }).$mount('#app')
//     // }
//     const uid = user.uid;
//     console.log(uid)
//   } else {
//     console.log('Не авторизованный пользователь')
//   }
// });




Vue.config.productionTip = false

Vue.use(VueRouter);



new Vue({
  store,
  router,
  vuetify,
  provide() {
    return {
      $dayjs: dayjs
    }
  },
  render: h => h(App),
}).$mount('#app')






export { database };

// npm run serve
