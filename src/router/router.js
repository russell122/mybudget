import VueRouter from 'vue-router';

import Main from '../pages/Main';
import Auth from '../pages/Auth';
import Registration from '../pages/Registration';
import AddIncome from '../pages/AddIncome';
import AddExpenses from '../pages/AddExpenses';
import BalanceOut from '../pages/BalanceOut';
import IncomeCategorySettings from '../pages/IncomeCategorySettings';
import ExpensesCategorySettings from '../pages/ExpensesCategorySettings';
import ChartIncome from '../pages/ChartIncome';
import ChartExpenses from '../pages/ChartExpenses';
import AllIncome from '../pages/AllIncome';
import AllExpenses from '../pages/AllExpenses';
import NotFound from '../pages/404';

export default new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/Auth',
			name: 'Auth',
			meta: { layout: 'auth' },
			component: () => import('../pages/Auth.vue')
		},
		{
			path: '/Registration',
			name: 'Registration',
			meta: { layout: 'registration' },
			component: () => import('../pages/Registration.vue')
		},
		{
			path: '/',
			name: 'Main',
			meta: { layout: 'main' },
			component: () => import('../pages/Main.vue')
		},
		{
			path: '/ChartIncome',
			name: 'ChartIncome',
			meta: { layout: 'main' },
			component: () => import('../pages/ChartIncome.vue')
		},
		{
			path: '/ChartExpenses',
			name: 'ChartExpenses',
			meta: { layout: 'main' },
			component: () => import('../pages/ChartExpenses.vue')
		},
		{
			path: '/AllIncome',
			name: 'AllIncome',
			meta: { layout: 'main' },
			component: () => import('../pages/AllIncome.vue')
		},
		{
			path: '/AllExpenses',
			name: 'AllExpenses',
			meta: { layout: 'main' },
			component: () => import('../pages/AllExpenses.vue')
		},
		{
			path: '/IncomeCategorySettings',
			name: 'IncomeCategorySettings',
			meta: { layout: 'main' },
			component: () => import('../pages/IncomeCategorySettings.vue')
		},
		{
			path: '/ExpensesCategorySettings',
			name: 'ExpensesCategorySettings',
			meta: { layout: 'main' },
			component: () => import('../pages/ExpensesCategorySettings.vue')
		},
		{
			path: '/AddIncome',
			name: 'AddIncome',
			meta: { layout: 'main' },
			component: () => import('../pages/AddIncome.vue')
		},
		{
			path: '/AddExpenses',
			name: 'AddExpenses',
			meta: { layout: 'main' },
			component: () => import('../pages/AddExpenses.vue')
		},
		{
			path: '/BalanceOut',
			name: 'BalanceOut',
			meta: { layout: 'main' },
			component: () => import('../pages/BalanceOut.vue')
		},
		{
			path: '*',
			name: 'NotFound',
			component: NotFound
		},
	]
})